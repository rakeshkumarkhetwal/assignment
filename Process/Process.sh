#!/bin/bash/
path=/etc/utilityprocess.conf
CommandRanByPID(){
	echo "Showing the command ran by the process '$pid' "
	awk '{if($2=='$pid') print $11 $12 }' $path | tee -a /var/log/process.log
}

EnvirVarUsed(){
	echo "environment variable used by the process $pid"
	sudo cat /proc/$pid/environ | tee -a /var/log/process.log
	
}

DetailedInfo(){
	echo " Detailed info about the process $pid"
	awk '{if($2=='$pid') print $0 }' $path | tee -a /var/log/process.log

}

n=$(wc -l $path | cut -d " " -f 1)


for(( i=2; i<=$n; i++ ))
do	
	pid=$(awk 'NR=='$i' {print $2 }' $path) 
	CommandRanByPID $pid
	EnvirVarUsed $pid
	DetailedInfo $pid
	
done




